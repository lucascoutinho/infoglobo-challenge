import warnings

from peewee import SqliteDatabase

from infoglobo import models

models.database = SqliteDatabase(':memory:')
warnings.filterwarnings('ignore', category=DeprecationWarning)

