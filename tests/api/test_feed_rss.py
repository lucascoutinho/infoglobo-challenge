from infoglobo.models import FeedRss
from tests.base_test import BaseTest
from tests.api.fixtures import fixture_user


class TestFeedRss(BaseTest):

    def test_should_convert_xml_to_json(self):
        with fixture_user() as user:
            self.create_headers(user)
            feed_rss = FeedRss.get()
            endpoint = '/feed_rss/{}'.format(feed_rss.id)
            response = self.app.get(endpoint, headers=self.headers)
            assert response.status_int == 200
            json_response = response.json
            assert 'feed' in json_response

    def test_should_not_found(self):
        with fixture_user() as user:
            self.create_headers(user)
            endpoint = '/feed_rss/1000'
            response = self.app.get(endpoint, headers=self.headers,
                                    expect_errors=True)
            assert response.status_int == 400
