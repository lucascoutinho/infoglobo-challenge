from contextlib import contextmanager

from infoglobo.models import User


@contextmanager
def fixture_user():
    user = User(name='test', password='test')
    user.save()
    try:
        yield user
    finally:
        pass
