from http import HTTPStatus

from infoglobo.api_helpers import rollback_user, parse_token
from infoglobo.models import User
from tests.base_test import BaseTest


class TestUserRegister(BaseTest):

    def test_should_receive_bad_request(self):
        response = self.app.post_json('/user', params={}, expect_errors=True)
        self.assertEqual(response.status_code, 400)

    def test_should_register_user(self):
        params = {
            'name': 'test',
            'password': 'password'
        }
        response = self.app.post_json('/user', params=params)
        self.assertEqual(response.status_code, 200)
        json_response = response.json
        self.assertEqual(json_response, {'id': 1})
        user = User.get(id=1)
        assert user.password != 'password'

    def test_should_get_integrity_error(self):
        params = {
            'name': 'test',
            'password': 'password'
        }
        response = self.app.post_json('/user', params=params)
        self.assertEqual(response.status_code, 200)
        response = self.app.post_json('/user', params=params,
                                      expect_errors=True)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {'message': 'User already exists'})
        assert User.select().count() == 1


class TestLoginUser(BaseTest):

    def test_should_return_token(self):
        params = {
            'name': 'test',
            'password': 'test',
        }
        User(**params).save()
        response = self.app.get('/user/login', params=params)
        json_response = response.json
        payload = parse_token('Bear ' + json_response['token'])
        user = rollback_user(payload)
        assert user.name == 'test'

    def test_should_return_error(self):
        params = {
            'name': 'test',
            'password': 'test',
        }
        User(**params).save()
        params['name'] = 'error'
        response = self.app.get('/user/login', params=params,
                                expect_errors=True)
        assert response.status_code == HTTPStatus.NOT_FOUND
        json_response = response.json
        assert json_response['message'] == 'Not found'

