from unittest import TestCase

from peewee import SqliteDatabase
from webtest import TestApp
from bottle import default_app


from infoglobo.api_helpers import create_token
from infoglobo import models
from infoglobo.feed_rss.queryset import insert_or_get_info_globo, \
    insert_generic_feeds
from infoglobo.models import User, FeedRss, XPATHFeed


app = default_app()


class BaseTest(TestCase):

    def __init__(self, *args, **kwargs):
        super(BaseTest, self).__init__(*args, **kwargs)
        self.app = TestApp(app)
        self.database = SqliteDatabase(':memory:')
        self.models = [User, FeedRss, XPATHFeed]
        models.proxy.initialize(self.database)

    def setUp(self):
        models.proxy.obj.create_tables(self.models)
        insert_or_get_info_globo()
        insert_generic_feeds()

    def tearDown(self):
        models.proxy.obj.drop_tables(self.models)

    def create_headers(self, user):
        token = create_token(user)
        self.headers = {'Authorization': 'Bear ' + token}
