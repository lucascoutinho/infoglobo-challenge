import json
import os

import lxml

from infoglobo.models import FeedRss
from infoglobo.parser import ParseRssToDict
from tests.base_test import BaseTest


class TestParser(BaseTest):

    def setUp(self):
        super().setUp()
        feed = FeedRss.get()
        self.parser = ParseRssToDict(feed.xpaths)

    def test_string_contains_html(self):
        assert self.parser._string_contains_html('<html><p></html>')

    def test_string_not_contains_html(self):
        assert self.parser._string_contains_html('html') is False

    def test_should_transform_to_dict(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file = os.path.join(dir_path, 'rss.xml')
        with open(file) as rss:
            parsed = self.parser.to_dict(rss.read().encode('utf-8'))
            assert 'feed' in parsed
            assert len(parsed['feed']) == 1

    def test_should_raise_error(self):
        with self.assertRaises(lxml.etree.XMLSyntaxError):
            self.parser.to_dict('<invalid>')

