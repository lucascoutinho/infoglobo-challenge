from typing import Iterator
import re

from lxml import html
from lxml import etree

from infoglobo.feed_rss.queryset import filter_xpath
from infoglobo.models import XPATHFeed

PARSER = 'html.parser'


TAG_RE = re.compile(r'<[^>]+>')


class ParseRssToDict:

    def __init__(self, xpaths: Iterator[XPATHFeed]):
        self.xpaths = xpaths

    @staticmethod
    def _string_contains_html(content):
        return html.fromstring(content).find('.//*') is not None

    def _parse_description(self, item):

        def _join(content):
            return ''.join(map(str.strip, content))

        def _unify(content):
            return list(set(content))

        description = item.xpath(filter_xpath(self.xpaths,
                                              XPATHFeed.DESCRIPTION))
        full_description = ''.join(description)
        if not self._string_contains_html(full_description):
            return full_description

        try:
            fragment = html.fragment_fromstring(full_description,
                                                create_parent=True)
            description = list()
            media_types = {
                'text': (XPATHFeed.DESCRIPTION_TEXT, _join),
                'images': (XPATHFeed.DESCRIPTION_IMAGE, _unify),
                'links': (XPATHFeed.DESCRIPTION_LINKS, _unify),
            }
            for media_type, field_fn in media_types.items():
                field, fn = field_fn
                contents = fragment.xpath(filter_xpath(self.xpaths, field))
                contents = map(str, contents)
                description.append({
                    'type': media_type,
                    'content': fn(contents),
                })
        except TypeError:
            return TAG_RE.sub('', full_description).strip()
        return description

    def _parse_link(self, item) -> str:
        return ''.join(item.xpath(filter_xpath(self.xpaths,
                                               XPATHFeed.LINK)))

    def _parse_title(self, item) -> str:
        return ''.join(item.xpath(filter_xpath(self.xpaths,
                                               XPATHFeed.TITLE)))

    def to_dict(self, xml: str) -> dict:
        rss = {'feed': []}
        tags_fn = {
            'title': self._parse_title,
            'description': self._parse_description,
            'link': self._parse_link,
        }
        tree = etree.fromstring(xml)
        for item in tree.iter('item'):
            node = {'item': {}}
            for tag, fn in tags_fn.items():
                node['item'][tag] = fn(item)
            rss['feed'].append(node)
        return rss

