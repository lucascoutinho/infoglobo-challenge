from datetime import datetime, timedelta
from functools import wraps
from http import HTTPStatus

from bottle import response, request, abort
import jwt

from infoglobo.consts import JWT_ALGORITHM, JWT_SECRET, JWT_EXPIRATION_DAYS
from infoglobo.models import User


def parse_token(token) -> str:
    """
    :return: payload:str
    """
    try:
        bearer, token = token.split(' ')
        return jwt.decode(token, JWT_SECRET, algorithm=JWT_ALGORITHM)
    except jwt.DecodeError:
        return None
    except ValueError:
        return None


def rollback_user(payload: dict) -> User:
    user_id = payload['sub']
    try:
        return User.get(id=user_id)
    except User.DoesNotExist:
        return None


def create_token(entity) -> bytes:
    payload = {
        # subject
        'sub': entity.id,
        # issued at
        'iat': datetime.utcnow(),
        # expiration
        'exp': datetime.utcnow() + timedelta(JWT_EXPIRATION_DAYS)
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return token.decode('utf-8')


def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        response.message = None
        token = request.headers.get('Authorization')
        if token:
            payload = parse_token(token)
            if payload:
                user = rollback_user(payload)
                if user:
                    return func(*args, **kwargs)
        response.status = HTTPStatus.UNAUTHORIZED
        return {'message': 'Unauthorized'}
    return wrapper
