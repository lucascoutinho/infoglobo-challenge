import os

current_path = os.path.dirname(os.path.abspath(__file__))
DATABASE_FILE = os.path.join(current_path, 'database.sqlite')

# JWT
JWT_EXPIRATION_DAYS = 365
JWT_ALGORITHM = 'HS256'
JWT_SECRET = os.getenv('JWT_SECRET', 'kl09,AlxkA0]^xZZawwlkvj911')
