import os
from bottle import default_app, run
from peewee import SqliteDatabase

from infoglobo.config import DEBUG, PORT
from infoglobo import models
from infoglobo.consts import DATABASE_FILE
from infoglobo.feed_rss.queryset import (
    insert_or_get_info_globo, insert_generic_feeds
)
from infoglobo.user.queryset import insert_default_test_user

db_exist = os.path.exists(DATABASE_FILE)
app = application = default_app()
database = models.proxy.initialize(SqliteDatabase(DATABASE_FILE))

#  Initialize database models
if not db_exist:
    for _model in models.MODELS:
        _model.create_table()

insert_or_get_info_globo()
insert_default_test_user()
insert_generic_feeds()

if __name__ == '__main__':
    run(app=app, host='0.0.0.0', port=PORT, debug=DEBUG, reloader=True)
