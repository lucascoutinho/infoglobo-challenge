from infoglobo.models import User


def insert_default_test_user():
    try:
        User.get(name='infoglobo')
    except User.DoesNotExist:
        User(name='infoglobo', password='test').save()
