import json
from http import HTTPStatus

from bottle import post, request, response, abort, get
from peewee import IntegrityError

from infoglobo.api_helpers import create_token
from infoglobo.models import User

__all__ = ['post_user', 'login_user']


def _get_json_request(method):
    name, password = method.get('name'), method.get('password')
    if not name or not password:
        message = {'message': 'Missing name or password'}
        abort(code=HTTPStatus.BAD_REQUEST, text=json.dumps(message))
    return name, password


@post('/user')
def post_user():
    name, password = _get_json_request(method=request.json)
    user = User(name=name, password=password)
    try:
        if user.save():
            return {
                'id': user.id,
            }
        else:
            response.status = HTTPStatus.BAD_GATEWAY
            return {'message': 'Internal error'}
    except IntegrityError:
        response.status = HTTPStatus.BAD_REQUEST
        return {'message': 'User already exists'}


@get('/user/login')
def login_user():
    name, password = _get_json_request(method=request.GET)
    hashed_password = User.hash_password(password)
    check_password = User.verify_password(hashed_password, password)
    if check_password:
        try:
            user = User.get(name=name)
            return {'token': create_token(user)}
        except User.DoesNotExist:
            pass
    response.status = HTTPStatus.NOT_FOUND
    return {'message': 'Not found'}

