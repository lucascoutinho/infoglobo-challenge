import logging
from logging import getLogger


LOGGERS = []


def get_logger(name):
    if name in LOGGERS:
        return LOGGERS[name]
    logger = getLogger(name)
    logger.setLevel(logging.DEBUG)
    return logger


