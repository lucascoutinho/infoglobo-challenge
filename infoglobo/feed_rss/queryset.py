from typing import Iterator

from infoglobo.models import FeedRss, XPATHFeed, DefaultXPATH


def insert_or_get_info_globo() -> FeedRss:
    try:
        return FeedRss.get(name='infoglobo')
    except FeedRss.DoesNotExist:
        feed_rss = FeedRss(name='infoglobo',
                           url='http://revistaautoesporte.globo.com/rss/'
                               'ultimas/feed.xml')
        feed_rss.save()
        fields_xpath = {
            XPATHFeed.TITLE: './title/text()',
            XPATHFeed.LINK: './link/text()',
            XPATHFeed.DESCRIPTION: './description/text()',
            XPATHFeed.DESCRIPTION_TEXT: '//p/text()',
            XPATHFeed.DESCRIPTION_LINKS: '//a/@href',
            XPATHFeed.DESCRIPTION_IMAGE: '//img/@src'
        }
        for field, xpath in fields_xpath.items():
            XPATHFeed(feed=feed_rss, field=field, xpath=xpath).save()
        return feed_rss


def insert_generic_feeds() -> FeedRss:
    try:
        FeedRss.get(name='globo')
        FeedRss.get(name='uol')
    except FeedRss.DoesNotExist:
        FeedRss(name='globo', url='http://pox.globo.com/rss/g1/brasil/').save()
        FeedRss(name='uol', url='http://rss.home.uol.com.br/index.xml').save()


def filter_xpath(xpaths: Iterator[XPATHFeed], field) -> str:
    xpath = list(filter(lambda f: f.field == field, xpaths))
    if xpath:
        return next(iter(xpath)).xpath
    return DefaultXPATH.get(field)

