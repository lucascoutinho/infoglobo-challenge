from http import HTTPStatus

from bottle import response, get
from requests import HTTPError

from infoglobo.api_helpers import login_required
from infoglobo.fetcher import fetch_url
from infoglobo.models import FeedRss
from infoglobo.parser import ParseRssToDict

__all__ = ['get_feed_by_id', ]


@get('/feed_rss/<id_feed_rss>')
@login_required
def get_feed_by_id(id_feed_rss):
    try:
        feed_rss = FeedRss.get(id=int(id_feed_rss))
    except (FeedRss.DoesNotExist, TypeError):
        response.status = HTTPStatus.BAD_REQUEST
        return {'message': 'Bad request'}

    try:
        content = fetch_url(feed_rss.url)
        parser = ParseRssToDict(feed_rss.xpaths)
        return parser.to_dict(content)
    except HTTPError:
        response.status = HTTPStatus.BAD_GATEWAY
        return {'message': 'Bad gateway'}




