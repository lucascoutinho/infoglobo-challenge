from functools import lru_cache

import requests

from infoglobo.logger import get_logger

logger = get_logger(__name__)


@lru_cache(maxsize=16)
def fetch_url(url) -> bytes:
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.content
    except requests.HTTPError as e:
        logger.exception(e)
        raise

