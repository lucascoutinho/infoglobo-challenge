from passlib.context import CryptContext
from peewee import Model, CharField, ForeignKeyField, IntegerField, Proxy


pwd_context = CryptContext(
    schemes=["pbkdf2_sha256"],
    default="pbkdf2_sha256",
    pbkdf2_sha256__default_rounds=10000,
)

proxy = Proxy()

MODELS = []


def register(cls):
    MODELS.append(cls)
    return cls


class BaseModel(Model):
    class Meta:
        database = proxy


@register
class User(BaseModel):

    name = CharField(null=False, unique=True)
    password = CharField(null=False)

    @staticmethod
    def hash_password(password: str):
        return pwd_context.encrypt(password)

    @staticmethod
    def verify_password(hashed, password):
        return pwd_context.verify(password, hashed)

    def save(self, force_insert=False, only=None):
        self.password = self.hash_password(self.password)
        return super(User, self).save(force_insert, only)


@register
class FeedRss(BaseModel):

    name = CharField(null=False)
    url = CharField(null=False)

    def __repr__(self):
        return '{}, {}'.format(self.name, self.url)

    def __str__(self):
        return self.__repr__()


@register
class XPATHFeed(BaseModel):

    class Meta:
        indexes = (
            (('feed', 'field'), True),  # defining unique fields
        )

    TITLE = 1
    LINK = 2
    DESCRIPTION = 3
    # For html in description
    DESCRIPTION_TEXT = 4
    DESCRIPTION_IMAGE = 5
    DESCRIPTION_LINKS = 6

    FIELDS = (
        (TITLE, 'Title'),
        (LINK, 'Image'),
        (DESCRIPTION_TEXT, 'Description text'),
        (DESCRIPTION_IMAGE, 'Description image'),
        (DESCRIPTION_LINKS, 'Description link'),
    )

    feed = ForeignKeyField(FeedRss, related_name='xpaths')
    field = IntegerField(choices=FIELDS)
    xpath = CharField(null=False)


DefaultXPATH = {
    XPATHFeed.TITLE: './title/text()',
    XPATHFeed.LINK: './link/text()',
    XPATHFeed.DESCRIPTION: './description/text()',
}

