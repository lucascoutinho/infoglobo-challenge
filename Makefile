run:
	python -m challenge
run_docker:
	docker build -t infoglobo_challenge .
	docker run -p 8000:8000 infoglobo_challenge
test:
	python  -W ignore::DeprecationWarning -m unittest discover
